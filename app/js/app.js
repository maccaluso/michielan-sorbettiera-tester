const $ = require('jquery')
const osc = require('osc')

// Create an osc.js UDP Port listening on port 57121.
var udpPort = new osc.UDPPort({
    localAddress: "0.0.0.0",
    localPort: 57121,
    metadata: true
});


udpPort.on("message", function (oscMessage, timeTag, info) {
    $('#feedback').text( oscMessage.address.replace('/','').replace('-',' ') );
});

udpPort.on("error", function (error) {
    console.log("An error occurred: ", error.message);
});

// Open the socket.
udpPort.open();


//When the port is read, send an OSC message to, say, SuperCollider
udpPort.on("ready", function () {
    $('#feedback').text('not rolling');
});